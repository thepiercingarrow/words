---
title: In The Night
author: thepiercingarrow (tpa)
date: '2018-09-26'
categories:
  - short-stories
tags:
  - drama
---
## In The Night

Two shots rang out almost simultaneously – the shorter boy's body crumpled to the floor. A taller boy stepped onto the dirt path. He stooped down, his hands removing the mask from the fallen body's head. He gasped as he recognized the unmoving face. The dead boy's unblinking, blank eyes seemed to bore into the taller boy's skull. The taller boy blinked with recognition. He let out a cry of disbelief, and then a high pitched wail, one that only a female could have produced. He tilted his hood back to wipe his tears with his left arm, and his face partook the moonlight. Rather, her face. There stood an agonized Wildkat, face tortured beyond recognition.

She stumbled, and as her legs failed her, she collapsed over the shorter boy's bleeding body. The tears split from her eyes, pouring over him. The blood, initially spurting from his wounds, slowed to a quiet but ominous seeping, meeting the tears and forming a cold, harrowing pool of anguish. A passing cloud covered the moon, and as darkness crept into the scene, the bodily warmth of the two lovers departed into the air. Wildkat began to asphyxiate.

By the time villagers found the two bodies the following morning, the earth had sucked out the ocean of agony deep within its pores. Two guns were recovered near the bodies, and it was naturally assumed that a lethal fight had occurred. The only evidence otherwise were the mingled drops of blood and tears, the amalgamation of willing sacrifices slowly stealing to the heavens.
